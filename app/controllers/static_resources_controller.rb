require 'zip'
require 'rgeo/shapefile'
require 'fileutils'

ALLOWED_EXTENSIONS = [".shp", ".shx", ".dbf", ".prj"]

class StaticResourcesController < ApplicationController
  def geosampa_data
    @platforms = Platform.all.map{|p| [p.url, p.id]}
  end

  def create
    begin
      url = "/tmp/#{params[:file_url]}"

      ::Zip::File.open("#{url}.zip") do |zip_files|
        puts "deletando arquivo..."
        zip_files.each do |x|
          if ALLOWED_EXTENSIONS.include?(x.name.last(4))
            FileUtils.rm("#{url + x.name.last(4)}", force: true)
            x.extract("#{url + x.name.last(4)}")
          end
        end
      end

      puts "Opening file..."
      ::RGeo::Shapefile::Reader.open("#{url}.shp") do |file|
        file.each do |record|
          puts "RECORD =>"
          puts record.inspect
          record.geometry.coordinates

          attrs = StaticResource.fetch(file)
          attrs["identifier"] = params[:unique_id]
          sr = StaticResource.create(attrs)
          sr.send_data(record.geometry.coordinates)
        end

      end

    rescue => e
      puts "ERROR: #{e}"
    end
    flash[:success] = "Data gathered!"

    redirect_to "/"
  end
end
